function formatTime(date){
    return new Intl.DateTimeFormat(window.locale, {
        hour: "2-digit",
        minute: "numeric"
    }).format(date)
}

function getDayName(date){
    return new Intl.DateTimeFormat(window.locale, {
        weekday: 'long',
        month: "long",
        day: "numeric"
    }).format(date);
}

function DayEvent(date, color, description, fullInfo, location, startTime, endTime){
    this.color = color
    this.description = description
    this.date = new Date(date)
    this.startTime = startTime != null ? new Date(startTime) : startTime
    this.endTime = endTime != null ? new Date(endTime) : endTime

    this.fullInfo = fullInfo
    this.location = location

    this.startDisplay = startTime != null ? formatTime(startTime) : "No start"
    this.endDisplay = endTime != null ? formatTime(endTime) : "No end"
    this.dateDisplay = getDayName(date)


    this.fullDescription = function(){
        let full = ""

        full += startTime != null ? formatTime(startTime) : ""

        if (startTime != null && endTime != null){
            full += " - "
            full += full != null ? formatTime(endTime) : ""
        }

        if (full.length > 0){
            full += ": "
        }

        full += description ?? ""

        return full
    }
}


function getEvents(date){

    const events = []

    const y = date.getFullYear(), m = date.getMonth(), d = date.getDate()

    if (y == today.getFullYear() && m == today.getMonth() && d == today.getDate()){
        events.push(new DayEvent(date, "blue", "Inspecting the prototype", "Inspecting the students' submissions of the project prototype"))
    }

    if (date.getDay() == 1){
        events.push(new DayEvent(date, "red", "Psychology Lecture", "Psychology Lecture by Prof. Dr. Dirk Wentura", "B31", new Date(y, m, d, 10, 15), new Date(y, m, d, 11, 45)))
        events.push(new DayEvent(date, "red", "HCI Lecture", "Human Computer Interaction Lecture by Dr. Martin Schmitz", "E13", new Date(y, m, d, 12, 15), new Date(y, m, d, 13, 45)))
    }

    if (date.getDay() == 3 && date.getDate() % 2 == 0){
        events.push(new DayEvent(date, "red", "Maths Lecture", undefined, undefined, new Date(y, m, d, 10, 15), new Date(y, m, d, 11, 45)))
    }

    if ((date.getDate() + date.getDay()) % 3 == 0){
        events.push(new DayEvent(date, "blue", "Study Group Meetup", "Lets learn some maths", "Discord ", new Date(y, m, d, 16, 00)))
    }

    if (date.getDay() == 6){
        events.push(new DayEvent(date, "blue", "Party at Waldhaus", "Lets have some fun", "Waldhaus ", new Date(y, m, d, 19, 00)))
    }

    return events
}

function getEventTypes(){
    return [
        {
            name: "Event",
            color: "blue"
        },
        {
            name: "Lecture",
            color: "red"
        }
    ]
}


function Day(date, mainMonth){
    this.date = new Date(date)
    this.mainMonth = mainMonth
    this.events = getEvents(date)
    this.weekDayDisplay = new Intl.DateTimeFormat(window.locale, {
        weekday: 'short'
    }).format(date);
    this.dayOfMonthDisplay = new Intl.DateTimeFormat(window.locale, {
        day: "2-digit"
    }).format(date);
    this.today = date.getDate() == today.getDate() && date.getMonth() == today.getMonth() && date.getFullYear() == today.getFullYear()
}

function getMonthDisplay(year, month){

    let days = []
    let displayDays = []

    let startDay = new Date(year, month, 1);
    
    let currentDay = new Date(startDay);

    while (currentDay.getDay() != 1){
        currentDay.setDate(currentDay.getDate() - 1)
    }


    for (let i = 0; i < 6 * 7; i++) {

        const mainMonth = currentDay.getMonth() == month
        const day = new Day(currentDay, mainMonth)

        displayDays.push(day)
        if (mainMonth){
            days.push(day)
        }

        currentDay = new Date(currentDay);
        currentDay.setDate(currentDay.getDate() + 1)
    }
    return {
        days: days,
        displayDays: displayDays,
        name: new Intl.DateTimeFormat(window.locale, {
            month: "long",
            year: "numeric"
        }).format(startDay)
    }
}

function getUpcomingEvents(){

    const events = []


    const tod = new Date(today)
    tod.setDate(tod.getDate() + 2)
    
    let start = new Date(tod.getFullYear(), tod.getMonth(), tod.getDate(), 18, 45)

    events.push(new DayEvent(tod, "blue", "Asta Party", "Big Asta party", "E22", start))
    
    events.push(new DayEvent(new Date(2023, 1, 15), "blue", "First Exams", "Better be prepared", "Campus "))

    return events
    
}