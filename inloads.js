const addedTemplates = []

const loadThese = ["navItem.html", "calendar.html", "daypreview.html", "event.html", "upcomingEvents.html"]

for (const page of appProxy.pages) {
    loadThese.push(page + ".html")
}

const node = document.currentScript

for (const url of loadThese){
    $http.get(url, (content) => {
        const template = document.createElement("template")

        const urlSplit = url.split(/(\.|\/)/)

        template.id = urlSplit[urlSplit.length - 3]
        template.innerHTML = content
        node.after(template)

        if (url == loadThese[loadThese.length - 1]){
            window.setTimeout(() => {appProxy.$triggerChange()}, 100)
        }
    })
}