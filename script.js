var appProxy = $fuwuProxy()

appProxy.page = "home"

const today = new Date()
appProxy.calMon = today.getMonth();
appProxy.calYear = today.getFullYear();

appProxy.eventViewId = -1
appProxy.eventViewMod = -1
appProxy.previewDayName = "Today"

appProxy.buildingName = ""

appProxy.pages = [
    "home",
    "timetable",
    "campus",
    "events",
    "settings",
    "building"
]

appProxy.displayEvent = null


function navItem(page, icon, alignEnd){
    return {
        page: page,
        icon: icon,
        alignEnd: !!alignEnd
    }
}

appProxy.mainNavItems = [
    navItem("home", "home_app_logo"),
    navItem("timetable", "event_note"),
    navItem("campus", "map"),
    navItem("events", "local_activity"),
    navItem("settings", "settings", true)
]

window.locale =
    navigator.languages && navigator.languages.length
        ? navigator.languages[0]
        : navigator.language;

appProxy.$listeners.unshift(() => {
    window.location.hash = appProxy.page;

    if (appProxy.page == "home"){
        appProxy.calMon = today.getMonth();
        appProxy.calYear = today.getFullYear();
        appProxy.eventViewId = -1
    }

    if (appProxy.page != "events"){
        appProxy.displayEvent = null
    }
    
    if (appProxy.eventViewId >= 0){
        appProxy.eventViewMod = appProxy.eventViewId + 6 - appProxy.eventViewId % 7
    }
})

function hashChange(){
    let hash = window.location.hash
    if (hash.startsWith("#")){
        hash = hash.substring(1)
    }
    if (hash.length == 0){
        hash = "home"
    }
    appProxy.page = hash
}

hashChange()

window.addEventListener("hashchange", hashChange)

function getHeaderTitle(){
    return "StudyRoom" + (() => {
        switch (appProxy.page){
            case "timetable": return ": Your Timetable"
            case "settings": return ": Settings"
            case "campus": return ": Campus"
            case "building": return `: Location - ${appProxy.buildingName.slice(0, -1)}<span style="font-size: 24px;">${appProxy.buildingName.slice(-1)}</span>`
            case "events": {
                if (appProxy.displayEvent == null){
                    return ": Events"
                } else {
                    return `: Event - ${appProxy.displayEvent.description}`
                }
            }
            default: return ""
        }
    })()
}

function nextMonth(element, dir){
    element = element.parentElement.parentElement
    element.classList.add("disappear")
    window.setTimeout(() => {

        const newDate = new Date(appProxy.calYear, appProxy.calMon, 1)
        newDate.setMonth(newDate.getMonth() + dir)
        appProxy.calMon = newDate.getMonth()
        appProxy.calYear = newDate.getFullYear()
        appProxy.eventViewId = -1


        element.classList.remove("disappear")
        element.classList.add("appear")
        
        window.setTimeout(() => {
            element.classList.remove("appear")
        }, 0.2 * 1000)
    }, 0.175 * 1000)
}

function getUseDate(){
    if (appProxy.eventViewId >= 0){
        let date = getMonthDisplay(appProxy.calYear, appProxy.calMon).displayDays[appProxy.eventViewId].date
        appProxy.previewDayName = getDayName(date)
        return date
    } else {
        appProxy.previewDayName = "Today"
        return new Date(today)
    }
}

function iFrameAdd(iframe){
    let links = iframe.contentWindow.document.querySelectorAll("a")
    links.forEach(link => link.addEventListener("click", campusClick))
}

function openBuilding(href){
    const urlSplit = href.split(/(\.|\/)/)

    appProxy.buildingName = urlSplit[urlSplit.length - 3].toUpperCase()
    appProxy.page = "building"
}

function openDisplayEventLocation(){
    appProxy.buildingName = appProxy.displayEvent.location
    appProxy.page = "building"
}

function campusClick(event){
    event.preventDefault()
    let target = event.target.closest("a")
    if (target != null){
        openBuilding(target.href)
    }
}

function openEvent(event){
    appProxy.displayEvent = event
    appProxy.page = "events"
}